<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Artel\Support\Traits\MigrationTrait;

class PostsCreateTable extends Migration
{
    use MigrationTrait;

    public function up()
    {
        $this->createTable();

        $this->addForeignKey('Post', 'User');
    }

    public function down()
    {
        Schema::dropIfExists('posts');
    }

    public function createTable()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('content');
            $table->timestamps();
        });
    }
}
