<?php 

use Illuminate\Database\Seeder;
use App\Models;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Post::class)->create([
            'user_id' => factory(App\Models\User::class)->create()->id,
        ]);

    }
}
