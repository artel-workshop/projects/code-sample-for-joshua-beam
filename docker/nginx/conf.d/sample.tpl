server {
        root /home/${proj_name}/backend/public;
        index index.php index.html index.htm index.nginx-debian.html;
        server_name ${DOMAIN} ;
        location / {
                try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ \.php$ {
       include fastcgi_params;
       fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
       fastcgi_pass 127.0.0.1:9000;

        }

        location ~ /\.ht {
                deny all;
        }



}

