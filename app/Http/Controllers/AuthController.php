<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\CheckRestoreTokenRequest;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Http\Requests\Auth\RestorePasswordRequest;
use App\Services\UserService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request, UserService $service)
    {
        $user = $service->findBy('email', $request->input('email'));

        if (empty($user) || !Hash::check($request->input('password'), $user->password)) {
            return response()->json([
                'message' => 'Authorization failed'
            ], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json([
            'token' => $user->createToken($user->name)->plainTextToken,
            'user' => $user
        ]);
    }

    public function register(RegisterUserRequest $request, UserService $service)
    {
        $user = $service->create($request->onlyValidated());

        return response()->json([
            'token' => $user->createToken($user->name)->plainTextToken,
            'user' => $user
        ]);
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $user = $request->user();

        $user->tokens()->delete();

        $token = $user->createToken($user->name)->plainTextToken;

        return response()->json([ 'token' => $token ]);
    }

    public function forgotPassword(ForgotPasswordRequest $request, UserService $service)
    {
        $service->forgotPassword($request->input('email'));

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function restorePassword(RestorePasswordRequest $request, UserService $service)
    {
        $service->restorePassword(
            $request->input('token'),
            $request->input('password')
        );

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function checkRestoreToken(CheckRestoreTokenRequest $request)
    {
        return response('', Response::HTTP_NO_CONTENT);
    }
}
