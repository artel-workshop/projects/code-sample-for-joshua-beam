<?php

namespace App\Http\Requests\Posts;

use App\Http\Requests\Request;

class CreatePostRequest extends Request
{
    public function rules(): array
    {
        return [
            'user_id' => 'integer|exists:users,id|required',
            'title' => 'string|required',
            'content' => 'string|required',
        ];
    }
}
