<?php

namespace App\Http\Requests\Posts;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Services\PostService;
use App\Http\Requests\Request;

class UpdatePostRequest extends Request
{
    public function rules(): array
    {
        return [
            'user_id' => 'integer|exists:users,id|required',
            'title' => 'string',
            'content' => 'string',
        ];
    }

    protected function prepareForValidation()
    {
        $service = app(PostService::class);

        if (!$service->exists($this->route('id'))) {
            throw new NotFoundHttpException(__('validation.exceptions.not_found', ['entity' => 'Post']));
        }
    }
}