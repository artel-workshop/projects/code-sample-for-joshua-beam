<?php

namespace App\Repositories;

use App\Models\Post;

/**
 * @property Post $model
 */
class PostRepository extends Repository
{
    public function __construct()
    {
        $this->setModel(Post::class);
    }
}
