<?php

namespace App\Models;

use Artel\Support\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use ModelTrait;

    protected $fillable = [
        'user_id',
        'title',
        'content',
    ];

    protected $hidden = ['pivot'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
