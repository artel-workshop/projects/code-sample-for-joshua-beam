<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Artel\Support\Traits\ModelTrait;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, ModelTrait, HasApiTokens;

    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id'
    ];

    protected $guarded = [
        'reset_password_hash'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'reset_password_hash'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function post()
    {
        return $this->hasOne(Post::class);
    }

}
