<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Artel\Support\Services\EntityService;
use App\Repositories\PostRepository;

/**
 * @mixin PostRepository
 * @property PostRepository $repository
 */
class PostService extends EntityService
{
    public function __construct()
    {
        $this->setRepository(PostRepository::class);
    }

    public function search($filters)
    {
        return $this
            ->with(Arr::get($filters, 'with', []))
            ->withCount(Arr::get($filters, 'with_count', []))
            ->searchQuery($filters)
            ->filterBy('user_id')
            ->filterByQuery(['title', 'content'])
            ->getSearchResults();
    }
}
