<?php

namespace App\Services;

use App\Jobs\SendMailJob;
use App\Mails\ForgotPasswordMail;
use App\Models\Role;
use Illuminate\Support\Arr;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Artel\Support\Services\EntityService;
use Illuminate\Support\Facades\Mail;

/**
 * @property UserRepository $repository
 */
class UserService extends EntityService
{
    protected $roleRepository;

    public function __construct()
    {
        $this->setRepository(UserRepository::class);
    }

    public function search($filters)
    {
        return $this->repository
            ->searchQuery($filters)
            ->filterBy('role_id')
            ->filterByQuery(['name', 'email'])
            ->getSearchResults();
    }

    public function create($data)
    {
        $data['role_id'] = Arr::get($data, 'role_id', Role::USER);
        $data['password'] = Hash::make($data['password']);

        return $this->repository->create($data);
    }

    public function update($where, $data)
    {
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        return $this->repository->update($where, $data);
    }

    public function forgotPassword($email)
    {
        $hash = $this->generateUniqueHash();

        $this->repository
            ->force()
            ->update([
                'email' => $email
            ], [
                'reset_password_hash' => $hash
            ]);

        Mail::queue(new ForgotPasswordMail($email, ['hash' => $hash]));
    }

    public function restorePassword($token, $password)
    {
        $this->repository
            ->force()
            ->update([
                'reset_password_hash' => $token
            ], [
                'password' => Hash::make($password),
                'reset_password_hash' => null
            ]);
    }

    protected function generateUniqueHash($length = 16)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
