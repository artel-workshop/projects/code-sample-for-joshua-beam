INSERT INTO roles(id, name, created_at, updated_at) VALUES
  (1, 'user', '2016-10-20 11:05:00', '2016-10-20 11:05:00');

INSERT INTO users(id, name, email, password, role_id, reset_password_hash, created_at, updated_at) VALUES
  (1, 'Gregg Stokes', 'tara15@example.com', 1, 2, 1, '2016-10-20 11:05:00', '2016-10-20 11:05:00');

INSERT INTO posts(id, user_id, title, content, created_at, updated_at) VALUES
  (1, 1, 'sed', 'inventore', '2016-10-20 11:05:00', '2016-10-20 11:05:00');

