<?php

namespace App\Tests;

use App\Models\User;
use Illuminate\Contracts\Console\Kernel;
use Artel\Support\Tests\TestCase as BaseTestCase;
use Artel\Support\AutoDoc\Tests\AutoDocTestCaseTrait;
use Illuminate\Contracts\Auth\Authenticatable;

abstract class TestCase extends BaseTestCase
{
    use AutoDocTestCaseTrait;
    protected $prepareSequencesExceptTables = ['migrations', 'password_resets', 'settings'];

    protected string $token;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->loadEnvironmentFrom('.env.testing');
        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    public function actingAs(Authenticatable $user, $driver = null)
    {
        parent::actingAs($user, $driver);

        $this->token = $user->createToken($user->name)->plainTextToken;

        $this->withHeaders(['Authorization' => 'Bearer ' . $this->token]);

        return $this;
    }
}
