# Laravel empty project

### Local run

```
   cp .env.example .env
   docker-compose up
   docker exec -it code-sample-for-joshua-beam_api_1 composer install
   docker exec -it code-sample-for-joshua-beam_api_1 /app/vendor/phpunit/phpunit/phpunit /app/tests
   docker exec -it code-sample-for-joshua-beam_api_1 php artisan migrate
   docker exec -it code-sample-for-joshua-beam_api_1 php artisan db:seed
```

After that you can check API in http://localhost