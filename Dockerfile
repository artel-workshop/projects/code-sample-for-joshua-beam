FROM artelworkshop/default-php8.1:1.0

RUN rm -rf /app/storage/logs
RUN mkdir /app 
WORKDIR /app
COPY . /app
RUN composer install
USER root
RUN ln -s /app/ /home/${proj_name}
RUN chown -R www-data:www-data /app
RUN echo "upload_max_filesize = 100M \n post_max_size = 100M" >> /usr/local/etc/php/conf.d/docker-fpm.ini
EXPOSE 9000
